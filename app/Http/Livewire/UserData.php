<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Http;

class UserData extends Component
{
    public function render()
    {
        
        $response = Http::get('https://firestore.googleapis.com/v1/projects/sitesurveytool2/databases/(default)/documents/users/');
        $doc = json_decode($response, true);

        $final_data = array_column($doc['documents'],'fields');
        // dd($final_data);
        return view('livewire.user-data',[
            'datas' => $final_data
        ]);
    }
}
