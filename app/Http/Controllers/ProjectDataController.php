<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use Barryvdh\DomPDF\Facade\Pdf;

class ProjectDataController extends Controller {
    
    public function index($userid,$docid) {

        $response = Http::get('https://firestore.googleapis.com/v1/projects/sitesurveytool2/databases/(default)/documents/users/'.$userid.'/projects/'.$docid.'/project-data');
        $doc = json_decode($response, true);

        $user_response = Http::get('https://firestore.googleapis.com/v1/projects/sitesurveytool2/databases/(default)/documents/users/'.$userid);
        $user_data = json_decode($user_response, true);
        // dd($user_data['fields']);

        $project_response = Http::get('https://firestore.googleapis.com/v1/projects/sitesurveytool2/databases/(default)/documents/users/'.$userid.'/projects/'.$docid);
        $project_information = json_decode($project_response, true);

        $final_data = array_column($doc['documents'],'fields');
        $final_user_data = $user_data['fields'];
        $final_project_information = $project_information['fields'];
        // dd($final_project_information);

        $fileName = $userid.'_'.$docid;
        $data = [
            'datas' => array_reverse($final_data),
            'user_information'=> $final_user_data,
            'project_information' => $final_project_information
        ];

        // dd($data);
        
        // $pdf = PDF::loadView('pdf', $data);
        // return $pdf->stream();

        // return view('pdf');

        $pdf = PDF::loadView('pdf', $data);
        Storage::put('/'.$fileName.'.pdf', $pdf->output());

        $reportPath = asset('storage/'.$fileName.'.pdf');
        return response()->json($reportPath, 200);

    }

    public function test_api() {
        $response = Http::get('https://firestore.googleapis.com/v1/projects/sitesurveytool2/databases/(default)/documents/users/3KvAlMTz46bhthnKVeedfKsjBRc2/projects/Project_9999/project-data');
        $doc = json_decode($response, true);
        dd($doc);
        // return $response;
    }

}
