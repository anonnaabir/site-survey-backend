<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>

table.GeneratedTable {
  width: 100%;
  background-color: #ffffff;
  border-collapse: collapse;
  border-width: 1px;
  border-color: #000000;
  border-style: solid;
  color: #000000;
}

table.GeneratedTable td, table.GeneratedTable th {
  border-width: 1px;
  border-color: #000000;
  border-style: solid;
  padding: 5px;
  text-align: center;
}

table.GeneratedTable thead {
  color: #ffffff;  
  background-color: #000000;
}

.center {
  display: block;
}

.center img {
  margin-left: 290px;
  margin-top: 10px;
}

.align {
  text-align: center;
  font-size: 18px;
  /* margin-top: -10px; */
  margin-bottom: 15px;
}

.project-information {
  margin-top: 10px;
  margin-bottom: 25px;
}


</style>

</head>
<body>
  <div class="center">
  <img class="logo" src="{{implode($user_information['companyLogo'])}}" width="100" height="100">
  <div class="project-information">
  <p class="align">{{implode($user_information['projectNameLabel'])}}: {{implode($project_information['name'])}}</p>
  <p class="align">{{implode($user_information['projectDescLabel'])}}: {{implode($project_information['location'])}}</p>
  </div>
  
  </div>
<table class="GeneratedTable">
  <thead>
    <tr>
      <th>{{implode($user_information['locationNameLabel'])}}</th>
      <th>{{implode($user_information['locationDescLabel'])}}</th>
      <th>Image</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($datas as $data)
    <tr>
        <td>{{implode($data['location_name'])}}</td>
        <td>{{implode($data['location_description'])}}</td>
        <td><img src="{{implode($data['location_image'])}}" width="250" height="300"></td>
    </tr>
    @endforeach
  </tbody>
</table>

</html>