<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
<table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
<thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
<tr>
<th scope="col" class="px-6 py-3">Name</th>
<th scope="col" class="px-6 py-3">Email</th>
<th scope="col" class="px-6 py-3">ID</th>
<th scope="col" class="px-6 py-3">Subscription</th>
<th scope="col" class="px-6 py-3">Projects</th>
</tr>
</thead>
<tbody>
    
@foreach ($datas as $data)
        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
        <th scope="row" class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
        {{implode($data['name'])}}
        </th>
        <td class="px-6 py-4">{{implode($data['email'])}}</td>
        <td class="px-6 py-4">{{implode($data['userid'])}}</td>
        <td class="px-6 py-4">{{implode($data['usertype'])}}</td>
        <td class="px-6 py-4">{{implode($data['projects'])}}</td>
    </tr>
@endforeach

</tr>
</tbody>
</table>
</div>
